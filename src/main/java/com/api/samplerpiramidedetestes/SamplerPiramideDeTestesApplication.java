package com.api.samplerpiramidedetestes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SamplerPiramideDeTestesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamplerPiramideDeTestesApplication.class, args);
	}

}
