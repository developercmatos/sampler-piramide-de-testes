package com.api.samplerpiramidedetestes.controller;

import com.api.samplerpiramidedetestes.dto.DadosCadastroMedico;
import com.api.samplerpiramidedetestes.entities.Medico;
import com.api.samplerpiramidedetestes.repository.MedicoRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

    @Autowired
    private MedicoRepository medicoRepository;

    @PostMapping
    @Transactional
    public void cadastrar(@RequestBody @Valid DadosCadastroMedico dados) {
        medicoRepository.save(new Medico(dados));

    }
}
