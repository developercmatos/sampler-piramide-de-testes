package com.api.samplerpiramidedetestes.dto;

public enum Especialidade {

    CARDIOLOGIA,
    DERMATOLOGIA,
    GINECOLOGIA,
    PEDIATRIA,
    PSIQUIATRIA,
    UROLOGIA;
}
