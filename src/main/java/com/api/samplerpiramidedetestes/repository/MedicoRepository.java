package com.api.samplerpiramidedetestes.repository;

import com.api.samplerpiramidedetestes.entities.Medico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicoRepository extends JpaRepository<Medico, Long>{

}
