ALTER TABLE medicos
DROP COLUMN nova_coluna;

ALTER TABLE medicos
    ADD COLUMN telefone VARCHAR(20) NOT NULL;