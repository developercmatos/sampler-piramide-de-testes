CREATE TABLE medicos (
                         id BIGINT PRIMARY KEY AUTO_INCREMENT,
                         nome VARCHAR(255) NOT NULL,
                         email VARCHAR(255) NOT NULL,
                         crm VARCHAR(255) NOT NULL,
                         especialidade VARCHAR(255) NOT NULL,
                         logradouro VARCHAR(255) NOT NULL,
                         bairro VARCHAR(255) NOT NULL,
                         cep VARCHAR(8) NOT NULL,
                         numero VARCHAR(255),
                         complemento VARCHAR(255),
                         cidade VARCHAR(255) NOT NULL,
                         uf VARCHAR(255) NOT NULL
);